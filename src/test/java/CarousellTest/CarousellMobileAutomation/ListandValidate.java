package CarousellTest.CarousellMobileAutomation;

import org.testng.annotations.Test;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.touch.offset.PointOption;
import junit.framework.Assert;

import org.testng.annotations.BeforeClass;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;

public class ListandValidate {
	public static AppiumDriver driver;

	@BeforeClass
	public void beforeClass() {
		try {
			DesiredCapabilities capabilities = new DesiredCapabilities();
			capabilities.setCapability("platformVersion", "6.0");
			capabilities.setCapability("deviceName", "Pixel_API_23");
			capabilities.setCapability("platformName", "Android");
			//Place APK file in the Lib Directory
			capabilities.setCapability("app",
					System.getProperty("user.dir")+"//lib//Carousell-Android-App.apk");
			capabilities.setCapability("appPackage", "com.thecarousell.Carousell");
			capabilities.setCapability("appActivity", "com.thecarousell.Carousell.activities.WelcomeActivity");
			capabilities.setCapability("autoAcceptAlerts", true);
			driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
			PageFactory.initElements(new AppiumFieldDecorator(driver), this);
		} catch (MalformedURLException e) {

			e.printStackTrace();
		}
	}

	@AndroidFindBy(id = "com.thecarousell.Carousell:id/login_button")
	AndroidElement Login_Btn_Home;
	@AndroidFindBy(id = "com.thecarousell.Carousell:id/layout_username")
	AndroidElement Login_Uname;
	@AndroidFindBy(id = "com.thecarousell.Carousell:id/layout_password")
	AndroidElement Login_Pwd;
	@AndroidFindBy(id = "com.thecarousell.Carousell:id/login_button")
	AndroidElement Login_Btn;
	@AndroidFindBy(id = "com.thecarousell.Carousell:id/button_sell")
	AndroidElement Sell_Btn;
	@AndroidFindBy(id = "com.android.packageinstaller:id/permission_allow_button")
	AndroidElement Permission_Btn1;
	@AndroidFindBy(id = "com.thecarousell.Carousell:id/view_thumbnail")
	List<AndroidElement> Items;
	@AndroidFindBy(id = "com.thecarousell.Carousell:id/action_done")
	AndroidElement Next_Btn;
	@AndroidFindBy(id = "com.thecarousell.Carousell:id/search_field")
	AndroidElement Search_Btn;
	@AndroidFindBy(id = "com.thecarousell.Carousell:id/tv_title")
	List<AndroidElement> Filter_Options;
	@AndroidFindBy(xpath = ".//android.widget.EditText[contains(@content-desc,'title')]")
	AndroidElement Item_Title;
	@AndroidFindBy(xpath = ".//android.widget.EditText[contains(@text,'Price')]")
	AndroidElement Item_Price;
	@AndroidFindBy(xpath = ".//android.widget.RadioButton[contains(@text,'Used')]")
	AndroidElement Item_Condition;
	@AndroidFindBy(xpath = ".//android.widget.Switch[contains(@text,'Mailing & Delivery OFF')]")
	AndroidElement Meetup_Delivery;
	@AndroidFindBy(xpath = ".//android.widget.EditText[contains(@content-desc,'mailing_details')]")
	AndroidElement Meetup_Delivery_TextArea;
	@AndroidFindBy(xpath = ".//android.widget.EditText[contains(@content-desc,'description')]")
	AndroidElement Item_Description;
	@AndroidFindBy(id = "com.thecarousell.Carousell:id/button_submit")
	AndroidElement List_It_Btn;
	@AndroidFindBy(xpath = ".//android.widget.TextView[contains(@text,'Successfully listed!')]")
	AndroidElement Successfully_listed_label;
	@AndroidFindBy(id = "com.thecarousell.Carousell:id/button_close")
	AndroidElement Close_Popup_Btn;

	@Test(priority = 1)
	public void login() {
		try {
			Login_Btn_Home.click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			Login_Uname.sendKeys("kbketan.bharadwaj11@gmail.com");
			Login_Pwd.sendKeys("Android6.0");
			Login_Btn.click();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@AndroidFindBy(xpath = ".//android.widget.TextView[@text='Promote your listing']")
	AndroidElement Promote_label;
	@AndroidFindBy(id = "com.thecarousell.Carousell:id/btn_close")
	AndroidElement Promote_Listing_Cls_btn;

	@Test(priority = 2)
	public void List_Item() {
		String Option_Text;
		String Item_Title_text = "Bicycle MW33";
		String Item_Price_Text = "600";
		String Delivery_Inst = "Delivery is free of cost";
		String Item_Description_Text = "Item is rarely used and in very good condition";
		WebDriverWait wait = new WebDriverWait(driver, 15);
		try {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			Sell_Btn.click();
			Permission_Btn1.click();
			Items.get(0).click();
			Next_Btn.click();
			Thread.sleep(4000);
			Search_Btn.sendKeys("Everything Else");
			int options_cnt = Filter_Options.size();
			for (int i = 0; i < options_cnt; i++) {
				Option_Text = Filter_Options.get(i).getText();
				if (Option_Text.equals("Everything Else")) {
					Filter_Options.get(i).click();
				}
			}
			Thread.sleep(3000);
			Item_Title.sendKeys(Item_Title_text);
			Item_Price.sendKeys(Item_Price_Text);
			Item_Condition.click();
			String selector = "Describe what you are selling and include any details a buyer might be interested in. People love items with stories!";
			scrollAndroid(selector);
			Meetup_Delivery.click();
			Meetup_Delivery_TextArea.sendKeys(Delivery_Inst);
			driver.hideKeyboard();
			Item_Description.click();
			Item_Description.sendKeys(Item_Description_Text);
			driver.hideKeyboard();
			Thread.sleep(5000);
			List_It_Btn.click();
			wait.until(ExpectedConditions.visibilityOf(Promote_label));
			Promote_Listing_Cls_btn.click();
			wait.until(ExpectedConditions.visibilityOf(Successfully_listed_label));
			Assert.assertEquals("Successfully listed!", Successfully_listed_label.getText());
			Close_Popup_Btn.click();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@AndroidFindBy(xpath = ".//android.widget.TextView[@text='BROWSE']")
	AndroidElement Browse_tab;
	@AndroidFindBy(xpath = ".//android.widget.TextView[contains(@text,'See All')]")
	AndroidElement See_All;
	@AndroidFindBy(xpath = ".//android.widget.TextView[@text='Property']")
	AndroidElement Category_Filter;
	@AndroidFindBy(xpath = ".//android.widget.TextView[@text='Everything Else']")
	AndroidElement Everything_Else_Filter_Opt;
	@AndroidFindBy(xpath = ".//android.widget.TextView[@text='OK, Got it!']")
	AndroidElement GotIt_Link;
	@AndroidFindBy(id = "com.thecarousell.Carousell:id/bar_filter_title")
	AndroidElement Sort_Filter;
	@AndroidFindBy(xpath = ".//android.widget.RadioButton[@content-desc='time_created,descending']")
	AndroidElement Recent_Filter_Option;
	@AndroidFindBy(id = "com.thecarousell.Carousell:id/btn_filter")
	AndroidElement Apply_Filter_Btn;
	@AndroidFindBy(xpath = ".//android.widget.RelativeLayout[@index='1']")
	AndroidElement prop_icon;
	@AndroidFindBy(xpath = ".//android.widget.TextView[contains(@text,'Bicycle MW33')]")
	AndroidElement result;

	@Test(priority = 3)
	public void validate_listing() {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		try {
			Browse_tab.click();
			wait.until(ExpectedConditions.visibilityOf(See_All));
			See_All.click();
			Thread.sleep(4000);
			scrollAndroid("Everything Else");
			Thread.sleep(10000);
			Everything_Else_Filter_Opt.click();
			GotIt_Link.click();
			Sort_Filter.click();
			Recent_Filter_Option.click();
			Apply_Filter_Btn.click();
			driver.manage().timeouts().implicitlyWait(6, TimeUnit.SECONDS);
			while (!locateElementWithoutException(result)) {
				scrollDown();
			}
			if (locateElementWithoutException(result)) {
				System.out.println("Item is Listed Successfully!");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// A Method to Scroll to an Element on Android Device
	public void scrollAndroid(String Element_Name) {
		try {
			driver.findElement(MobileBy.AndroidUIAutomator(
					"new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textMatches(\""
							+ Element_Name + "\").instance(0))"));
		} catch (Exception e) {
			e.printStackTrace();

		}

	}

	// locate element without exception
	public boolean locateElementWithoutException(AndroidElement el) {
		boolean isFound = false;
		try {
			if (el.isDisplayed())
				isFound = true;
		} catch (Exception e) {
			isFound = false;
		}
		return isFound;
	}

	// Method to Scroll Down Vertically
	private void scrollDown() {
		int pressX = driver.manage().window().getSize().width / 2;
		int bottomY = driver.manage().window().getSize().height * 4 / 5;
		int topY = driver.manage().window().getSize().height / 8;
		TouchAction touchAction = new TouchAction(driver);
		touchAction.longPress(PointOption.point(pressX, bottomY)).moveTo(PointOption.point(pressX, topY)).release()
				.perform();

	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}

}
